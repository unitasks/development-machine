#!/bin/bash
source "/vagrant/scripts/common.sh"

# Assumes that git is already installed
function cloneRepos {
    mkdir mkdir unitasks

    echo "Cloning unitasks wiki repo.."
    git clone https://gitlab.com/unitasks/wiki.git ~/unitasks/wiki

    echo "Cloning unitasks navigation page repo.."
    git clone https://gitlab.com/unitasks/navigation-page.git ~/unitasks/navigation-page

    echo "Cloning unitasks server repo.."
    git clone https://gitlab.com/unitasks/server.git ~/unitasks/server
}

cloneRepos
