# Development Machine for Unitasks

This repository contains the virtual machine setup that we use for development
via vagrant.

## Included Software

- Browser
    - Firefox
    - Chromium
- IDEs
    - Visual Studio Code
    - Pycharm Community
- Collaboration Software
    - Skype
    - Slack
- Tools
    - git
    - curl
    - postman

You will also find the following git repositories automatically cloned to
`~/unitasks/`:

- [Unitasks Navigation Page](https://gitlab.com/unitasks/navigation-page)
- [Unitasks Server](https://gitlab.com/unitasks/server)
- [Unitasks Wiki](https://gitlab.com/unitasks/wiki)

Remember to set your git config correctly (Name and E-Mail):

```sh
git config --global user.name "Your Name"
git config --global user.email your_gitlab_email@mail.tld
```

## Minimum System Requirements

If you want to use the Vagrant file as-is, you should have the following specs:

- >8GB RAM
- >4 CPU Cores
- 64 GB disk space

## Setup

- Install [Vagrant]() for your OS
- Install [VirtualBox]() for your OS
- Clone this repository and navigate to the root folder

Run the following command.

```sh
vagrant up && vagrant reload
```

Depending on your internet connection, this might take quite long. Ignore
the vm that pops up.

After the command has finished, you should see a standard ubuntu login
dialog.

- User: vagrant
- Password: vagrant

## Pause/Shutdown/Reinstall

- If you want to pause the VM, run `vagrant suspend`.
- If you want to shut the VM down, run `vagrant halt`.
- If you want to reinstall the VM, run `vagrant destroy && vagrant up`.